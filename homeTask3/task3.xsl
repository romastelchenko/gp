<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output method = "xml" indent="yes"/>
    
    <xsl:key name="AGROUP" match="item" use="substring(@Name,1,1)"/>
    <xsl:template match = "list">
        <list>
            <xsl:for-each select="item[generate-id(.) = generate-id(key('AGROUP',substring(@Name,1,1)))]">
                <xsl:sort select="substring(@Name,1,1)"/>
                <xsl:call-template name="item"/>
            </xsl:for-each>
        </list>
    </xsl:template>
    
    <xsl:template name="item">
            <capital value = "{substring(@Name,1,1)}">
                <xsl:for-each select="key('AGROUP',substring(@Name,1,1))">
                    <xsl:sort select = "@Name"/>
                    <name>
                        <xsl:value-of select="@Name"/>
                    </name>  
                </xsl:for-each>
            </capital>
            
        </xsl:template>
</xsl:stylesheet>