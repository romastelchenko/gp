<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    
        <xsl:key name="NODE" match="@*|*" use="name(.)"/>
    
    <xsl:template match="@*|*">
        <xsl:if test="generate-id(.) = generate-id(key('NODE',name(.)))">
            <xsl:value-of select="concat('&#x0A;','Node ' , &quot;'&quot; ,name(.) ,&quot;'&quot;,  ' found ', count(key('NODE',name(.))),' times.' )"/>
        </xsl:if>
        <xsl:apply-templates select="@*|*"/>      
    </xsl:template>

</xsl:stylesheet>