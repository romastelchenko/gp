<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ent="http://soapserver.com/entities"
    xmlns:requtil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.RequestUtil"
    exclude-result-prefixes="ent requtil">
    
    <xsl:output method="text"/>
    
    <xsl:template match="/">
        <xsl:variable name="CountryCode" select="ent:HotelsRequest/ent:CountryCode"/>
        <xsl:variable name="Lang" select="ent:HotelsRequest/ent:Lang"/>
        <xsl:variable name="HotelCategory" select="ent:HotelsRequest/ent:HotelCategory"/>
        <xsl:variable name="HotelName" select="ent:HotelsRequest/ent:HotelName"/>
        <xsl:variable name="CityName" select="ent:HotelsRequest/ent:CityName"/>
        
        <xsl:choose>
            <xsl:when test="string-length($CountryCode) = 0 or $Lang = 0">
                <xsl:text>Required fields are not filled</xsl:text>
                <xsl:message>
                    <xsl:value-of select="requtil:overrideResponse()"/>
                </xsl:message>
            </xsl:when>
            <xsl:otherwise>
        
        
        <xsl:text>select h.id as hotel_id, hn.name as hotel_name, h.code as hotel_code, h.phone as hotel_phone, h.latitude as hotel_latitude, h.longitude as hotel_longitude, h.check_in as check_in, h.check_out as check_out, h.url as hotel_url, h.fax as hotel_fax, l.id as country_id, l.code as country_code, ln.name as country_name, lcn.name as city_name, lcn.location_id as city_id from hotel h join hotel_name hn join location l join location_name ln join location_name lcn on h.country_id = l.id and h.city_id = lcn.location_id and l.id = ln.location_id and h.id = hn.hotel_id where l.code = "</xsl:text>    
        <xsl:value-of select="$CountryCode"/>
        <xsl:text>" and hn.lang_id = </xsl:text>
        <xsl:value-of select="$Lang"/>
        <xsl:text> and ln.lang_id = </xsl:text>
        <xsl:value-of select="$Lang"/>
        <xsl:text> and lcn.lang_id = </xsl:text>
        <xsl:value-of select="$Lang"/>
        
        <xsl:if test="string-length($CityName) &gt; 0">
            <xsl:text> and lcn.name = "</xsl:text>
            <xsl:value-of select="$CityName"/>
            <xsl:text>"</xsl:text>
        </xsl:if>
        <xsl:if test="string-length($HotelCategory) &gt; 0">
            <xsl:text> and h.category_id = "</xsl:text>
            <xsl:value-of select="$HotelCategory"/>
            <xsl:text>"</xsl:text>
         </xsl:if>
         <xsl:if test="string-length($HotelName) &gt; 0">
             <xsl:text> and hn.name like '%</xsl:text>
             <xsl:value-of select="$HotelName"/>
             <xsl:text>%'</xsl:text>
         </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
  
    
</xsl:stylesheet>