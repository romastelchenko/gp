<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ent="http://soapserver.com/entities"
    xmlns:sutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.StringUtil"
    xmlns:queryutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.SqlQueriesUtil"
    exclude-result-prefixes="ent sutil queryutil">
    
    <xsl:output method="xml"/>
    
    <xsl:template match="/">
        
        <xsl:variable name="images" select="queryutil:subQuery('select hi.hotel_id as image_hotel_id, hit.type_name as image_type, hi.url as image_url from hotel_images hi join hotel_image_type hit on hi.type_id = hit.id')"/>
        <xsl:variable name="cities_codes" select="queryutil:subQuery('select l.code as city_code, l.id as lcity_id from location l')"/>
        
        <ent:HotelsResponse>
            <ent:Hotels>
                <xsl:for-each select="Result/Entry">
                    <xsl:variable name="h_id" select="hotel_id"/>
                    <xsl:variable name="cit_id" select="city_id"/>            
                    <ent:Hotel Code = '{hotel_code}' Name = '{hotel_name}'>
                        <ent:Country Code = '{country_code}' Name = '{country_name}'/>
                        <ent:City Code = '{$cities_codes/Entry[lcity_id = $cit_id]/city_code}' Name  = '{city_name}'/>
                        <ent:Phone><xsl:value-of select="hotel_phone"/></ent:Phone>
                        <ent:Fax><xsl:value-of select="hotel_fax"/></ent:Fax>
                        <ent:Url><xsl:value-of select="hotel_url"/></ent:Url>
                        <ent:Check In = '{check_in}' Out = '{check_out}'/>
                        <ent:Coordinates>
                            <ent:Latitude><xsl:value-of select="hotel_latitude"/></ent:Latitude>
                            <ent:Longitude><xsl:value-of select="hotel_longitude"/></ent:Longitude>
                        </ent:Coordinates>
                        <ent:Images >  
                            <xsl:for-each select="$images/Entry[$h_id = image_hotel_id]">
                                <ent:Image Type = '{image_type}'>
                                    <ent:Url><xsl:value-of select="image_url"/></ent:Url>
                                </ent:Image>
                            </xsl:for-each>
                        </ent:Images >
                    </ent:Hotel>
                </xsl:for-each>  
             </ent:Hotels>
        </ent:HotelsResponse>
    </xsl:template>
    
</xsl:stylesheet>