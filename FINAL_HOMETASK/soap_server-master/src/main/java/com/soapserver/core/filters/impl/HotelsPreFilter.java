package com.soapserver.core.filters.impl;

import com.soapserver.core.filters.PreFilter;
import com.soapserver.core.processors.ServiceException;
import com.soapserver.entities.HotelsRequest;

public class HotelsPreFilter implements PreFilter<HotelsRequest>{

	@Override
	public boolean isApplicable(final HotelsRequest request) {
		return true;
	}
	
	@Override
	public void preProcess(final HotelsRequest request) throws ServiceException {	
		if (request.getCityName().length() > 0 && request.getCityName().length() < 2 ) {
			throw new ServiceException("Short city name");
		}
		if (request.getLang() > 20) {
			throw new ServiceException("Ну нет же столько языков");
		}
	}
}