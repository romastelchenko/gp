package com.soapserver.core.filters.impl;


import java.util.Iterator;

import com.soapserver.core.filters.PostFilter;
import com.soapserver.entities.CountryType;
import com.soapserver.entities.HotelsType;
import com.soapserver.entities.HotelType;
import com.soapserver.entities.ImagesType;
import com.soapserver.entities.HotelsResponse;
import com.soapserver.entities.HotelsRequest;

public class HotelsPostFilter implements PostFilter<HotelsRequest,HotelsResponse>{
	@Override
	public boolean isApplicable(final HotelsRequest request, final HotelsResponse response) {
		return true;
	}
	@Override
	public void postProcess(final HotelsRequest request, final HotelsResponse response) {
		HotelsType hotels = response.getHotels();
		
		for (final HotelType hotel : hotels.getHotel()) {
			hotel.setFax(hotel.getFax() + "_"+ hotel.getName());
			ImagesType images = hotel.getImages();
			for (final ImagesType.Image image : images.getImage()) {
				image.setUrl(image.getUrl().replace(' ', '_') + "_URL");
			}
			
		}
	}
	
}
