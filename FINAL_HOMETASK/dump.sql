-- MySQL dump 10.13  Distrib 5.7.20, for Win64 (x86_64)
--
-- Host: localhost    Database: hotels
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hotel`
--

DROP TABLE IF EXISTS `hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `city_id` bigint(20) NOT NULL,
  `country_id` bigint(20) NOT NULL,
  `chain_id` bigint(20) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `check_in` time NOT NULL,
  `check_out` time NOT NULL,
  `email` varchar(255) NOT NULL,
  `zip` varchar(50) DEFAULT NULL,
  `latitude` decimal(10,6) NOT NULL DEFAULT '0.000000',
  `longitude` decimal(10,6) NOT NULL DEFAULT '0.000000',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `default_image` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `category_id` enum('Hotel','Hostel','Apartment') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `city_id` (`city_id`),
  KEY `country_id` (`country_id`),
  CONSTRAINT `hotel_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `location` (`id`),
  CONSTRAINT `hotel_ibfk_3` FOREIGN KEY (`country_id`) REFERENCES `location` (`parent`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotel`
--

LOCK TABLES `hotel` WRITE;
/*!40000 ALTER TABLE `hotel` DISABLE KEYS */;
INSERT INTO `hotel` VALUES (1,'Hil',15,9,99,'+375 33 333 9999','fax','www.hilton.by','08:00:00','22:00:00','hiltonminsl@mail.ru','zip',23.111200,32.111200,1,1,'1999-03-22 15:30:55','Hotel'),(2,'MC',15,9,99,'+375 33 555 7890','fax','www.minskcity.by','10:00:00','24:00:00','minskcity@mail.ru','zip',23.111220,32.111220,1,1,'2001-05-24 17:00:00','Apartment'),(3,'HilM2',15,9,99,'+375 33 444 9999','fax','www.hilton.by/2','07:00:00','20:00:00','hiltonminsk2@mail.ru','zip',23.111330,32.111330,1,1,'2016-09-11 11:00:00','Hotel'),(4,'HSM',15,9,88,'+375 33 666 6666','fax','www.hotminsk.by','08:00:00','24:00:00','hotminsk@mail.ru','zip',23.111440,32.111440,1,1,'2015-11-03 09:00:00','Hostel'),(5,'Hst',15,9,99,'+375 44 677 7776','fax','www.hostel.by','07:00:00','22:00:00','hostel@mail.ru','zip',23.111550,32.111550,1,1,'2017-11-10 11:00:00','Hostel'),(6,'SM',15,9,99,'+375 33 555 5324','fax','www.minsksleep.by','08:00:00','20:00:00','minsksleep@mail.ru','zip',23.111600,32.111660,0,1,'2010-06-22 18:30:00','Hostel'),(7,'Pr',16,9,99,'+375 29 555 5555','fax','www.hotelmozyr.by','10:00:00','24:00:00','hotelmozyr@mail.ru','zip',23.222110,32.222110,1,1,'1990-12-12 12:22:33','Hotel'),(8,'HilL',19,11,99,'+333 21 111 222','fax','www.hiltonlondon.en','04:00:00','24:00:00','hiltonlondon@gmail.com','zip',22.111110,33.111110,1,1,'2010-07-22 10:00:00','Hotel'),(9,'HilL2',19,11,99,'+333 21 222 222','fax','www.hiltonlondon2.en','07:00:00','24:00:00','hiltonlondon2@gmail.com','zip',22.111220,33.111220,0,1,'2001-07-29 10:00:00','Hotel'),(10,'HilB',21,13,90,'+367 344 777 555','fax','www.hiltonberlin.en','06:00:00','23:00:00','hiltonberlin@gmail.com','zip',77.111110,88.111110,1,1,'2011-02-11 11:30:00','Hotel'),(11,'YO',21,13,90,'+367 12 888 555','fax','www.yohohoberlin.en','05:00:00','23:00:00','yohohoberlin@gmail.com','zip',77.111220,88.111220,1,1,'2013-11-10 13:00:00','Hotel'),(12,'HilP',22,12,99,'+123 45 33 77777','fax','www.hiltonparis.en','04:00:00','22:00:00','hiltonparis@gmail.com','zip',55.111110,66.111110,1,1,'2003-03-13 20:00:00','Hotel');
/*!40000 ALTER TABLE `hotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotel_image_type`
--

DROP TABLE IF EXISTS `hotel_image_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_image_type` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotel_image_type`
--

LOCK TABLES `hotel_image_type` WRITE;
/*!40000 ALTER TABLE `hotel_image_type` DISABLE KEYS */;
INSERT INTO `hotel_image_type` VALUES (1,'General'),(2,'Lobby'),(3,'Room'),(4,'Restaurant'),(5,'Аppearance');
/*!40000 ALTER TABLE `hotel_image_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotel_images`
--

DROP TABLE IF EXISTS `hotel_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_images` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hotel_id` bigint(20) NOT NULL,
  `type_id` tinyint(3) NOT NULL,
  `url` varchar(600) DEFAULT NULL,
  `thumbnail_url` varchar(600) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `hotel_images_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  CONSTRAINT `hotel_images_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `hotel_image_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotel_images`
--

LOCK TABLES `hotel_images` WRITE;
/*!40000 ALTER TABLE `hotel_images` DISABLE KEYS */;
INSERT INTO `hotel_images` VALUES (1,1,1,'hampton general','hampton general t'),(2,1,2,'hampton lobby','hampton lobby t'),(3,1,3,'hampton room','hampton room t'),(4,1,4,'hampton restaurant','hampton restaurant t'),(5,1,5,'hampton apperarance','hampton apperarance hampton t'),(6,7,1,'Pripyat','Pripyat t'),(7,7,2,'Pripyat lobby','Pripyat lobby t'),(8,7,3,'Pripyat room 1','Pripyat room 1 t'),(9,7,3,'Pripyat room 2','Pripyat room 2 t'),(10,8,1,'Hilton london ','Hilton london t'),(11,8,2,'Hilton london lobby','Hilton london lobby t'),(12,8,3,'Hilton london room','Hilton london room t'),(13,8,5,'Hilton london appearance','Hilton london app t'),(14,11,1,'YOHO','YOHO t'),(15,11,2,'YOHO lobby','YOHO lobby t'),(16,11,3,'YOHO room','YOHO room t'),(17,11,4,'YOHO restaurant','YOHO restaurant t'),(18,2,2,'Minsk city lobby','Minsk city lobby t'),(19,2,3,'Minsk city room','Minsk city room t'),(20,3,2,'Hilton 2 lobby ','Hilton 2 lobby t'),(21,3,3,'Hilton 2 room','Hilton 2 room t'),(22,4,1,'Hot hostel','Hot hostel t'),(23,5,1,'Hostel Minsk','Hostel Minsk t'),(24,6,1,'Sleep Minsk','Sleep Minsk t'),(25,9,1,'Hilton London 2','Hilton London 2 t'),(26,10,1,'Hilton Berlin','Hilton Berlin t'),(27,10,3,'Hilton Berlin room','Hilton Berlin room t'),(28,12,1,'Hilton Paris','Hilton Paris');
/*!40000 ALTER TABLE `hotel_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotel_name`
--

DROP TABLE IF EXISTS `hotel_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_name` (
  `hotel_id` bigint(20) NOT NULL,
  `lang_id` tinyint(3) NOT NULL,
  `name` varchar(500) NOT NULL,
  KEY `hotel_id` (`hotel_id`),
  KEY `lang_id` (`lang_id`),
  CONSTRAINT `hotel_name_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  CONSTRAINT `hotel_name_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `language` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotel_name`
--

LOCK TABLES `hotel_name` WRITE;
/*!40000 ALTER TABLE `hotel_name` DISABLE KEYS */;
INSERT INTO `hotel_name` VALUES (1,1,'Hampton by Hilton'),(1,2,'Хэмптон бай хилтон'),(2,1,'Minsk city '),(2,2,'Минск сити'),(3,1,'Hampton by Hilton 2'),(3,2,'Хэмптон бай хилтон 2'),(4,1,'Hot hostel'),(4,2,'Хот хостел'),(5,1,'Hostel Minsk'),(5,2,'Хостел минск'),(6,1,'Sleep Minsk'),(6,2,'Слип Минск'),(7,1,'Pripyat'),(7,2,'Припять'),(8,1,'Hampton by Hilton London'),(8,2,'Хэмптон бай Хилтон Лондон'),(9,1,'Hampton by Hilton London 2'),(9,2,'Хэмптон бай Хилтон Лондон 2'),(10,1,'Hilton'),(10,2,'Хилтон'),(11,1,'YOHO'),(11,2,'ЙОХО'),(12,1,'Hilton Paris'),(12,2,'Хилтон Париж');
/*!40000 ALTER TABLE `hotel_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) NOT NULL,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` VALUES (1,'en','english'),(2,'ru','русский'),(3,'de','deutsch'),(4,'es','espa?ol'),(5,'it','italiano'),(6,'fr','fran?ais');
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `type_id` tinyint(3) NOT NULL,
  `parent` bigint(20) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `laitude` decimal(10,6) NOT NULL DEFAULT '0.000000',
  `longitude` decimal(10,6) NOT NULL DEFAULT '0.000000',
  `time_zone` varchar(50) NOT NULL DEFAULT 'GMT',
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`),
  KEY `parent` (`parent`),
  CONSTRAINT `location_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `location_type` (`id`),
  CONSTRAINT `location_ibfk_2` FOREIGN KEY (`parent`) REFERENCES `location` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (7,'Root',1,NULL,1,23.434000,32.443000,'+3'),(9,'Blr',2,7,1,23.434000,32.443000,'+3'),(10,'Rus',2,7,1,44.555000,55.444000,'+3-+10'),(11,'Eng',2,7,1,22.333000,33.222000,'+0'),(12,'Fr',2,7,1,55.666000,66.555000,'+1'),(13,'Ger',2,7,1,77.880000,88.777000,'+2'),(14,'Ussr',2,7,0,11.100000,11.111000,'+1'),(15,'Mnsk',3,9,1,23.111000,32.111000,'+3'),(16,'Moz',3,9,1,23.222000,32.222000,'+3'),(17,'Brest',3,9,1,23.333000,32.333000,'+3'),(18,'Msq',3,10,1,44.111000,55.111000,'+3'),(19,'Lond',3,11,1,22.111000,33.111000,'+0'),(20,'Manch',3,11,1,22.222000,33.222000,'+0'),(21,'Berl',3,13,1,77.111000,88.111100,'+2'),(22,'Par',3,12,1,55.111000,66.110000,'+1');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location_name`
--

DROP TABLE IF EXISTS `location_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_name` (
  `location_id` bigint(20) NOT NULL,
  `lang_id` tinyint(3) NOT NULL,
  `name` varchar(255) NOT NULL,
  KEY `location_id` (`location_id`),
  KEY `lang_id` (`lang_id`),
  CONSTRAINT `location_name_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`),
  CONSTRAINT `location_name_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `language` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location_name`
--

LOCK TABLES `location_name` WRITE;
/*!40000 ALTER TABLE `location_name` DISABLE KEYS */;
INSERT INTO `location_name` VALUES (9,1,'Belarus'),(9,2,'Беларусь'),(9,3,'Wei?russland'),(9,4,'Bielorrusia'),(9,5,'Bielorussia'),(9,6,'B?larus'),(10,1,'Russia'),(10,2,'Россия'),(11,1,'Englang'),(11,2,'Англия'),(12,1,'France'),(12,2,'Франция'),(13,1,'Germany'),(13,2,'Германия'),(14,1,'USSR'),(14,2,'СCCР'),(15,1,'Minsk'),(15,2,'Минск'),(16,1,'Mozyr'),(16,2,'Мозырь'),(17,1,'Brest'),(17,2,'Брест'),(18,1,'Moscow'),(18,2,'Москва'),(19,1,'London'),(19,2,'Лондон'),(20,1,'Manchester'),(20,2,'Манчестер'),(21,1,'Berlin'),(21,2,'Берлин'),(22,1,'Paris'),(22,2,'Париж');
/*!40000 ALTER TABLE `location_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location_type`
--

DROP TABLE IF EXISTS `location_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_type` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location_type`
--

LOCK TABLES `location_type` WRITE;
/*!40000 ALTER TABLE `location_type` DISABLE KEYS */;
INSERT INTO `location_type` VALUES (1,'root'),(2,'country'),(3,'city');
/*!40000 ALTER TABLE `location_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-29 19:55:42
