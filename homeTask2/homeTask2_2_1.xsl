<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"> 
    
    <xsl:variable name="infoBorder" select = "'|'"/>
    <xsl:variable name="guestsBorder" select="'#'"/>
  
    <xsl:template match="/">  
        <string>
            <xsl:apply-templates select="Guests/Guest" />   
        </string>
    </xsl:template>
    
    <xsl:template match="Guest">
        <xsl:for-each select="child::*">
            <xsl:value-of select="concat(normalize-space(.),$infoBorder)"/>      
        </xsl:for-each> 
       <xsl:for-each select="@*">
           <xsl:choose>
               <xsl:when test="position() != last()">
                   <xsl:value-of select="concat(.,$infoBorder)" />
               </xsl:when>
               <xsl:otherwise>
                   <xsl:value-of select="." />
               </xsl:otherwise>
           </xsl:choose>
       </xsl:for-each>
        <xsl:if test="position() != last()">
            <xsl:value-of select="$guestsBorder"/>       
        </xsl:if>
        
    </xsl:template>
    
</xsl:stylesheet>