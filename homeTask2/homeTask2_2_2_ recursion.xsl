<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"> 
    <xsl:variable name="infoBorder" select = "'|'"/>
    <xsl:variable name="guestsBorder" select="'#'"/>
    
    <xsl:template match="/">
        <Guests>
            <xsl:call-template name="getGuests">
            <xsl:with-param name="string" select  = "."/>
        </xsl:call-template>
        </Guests>
        
    </xsl:template>
      
    <xsl:template name="getGuests">          
        <xsl:param name="string"/>
        <xsl:variable name="length" select="string-length($string)"/>
        
        <xsl:if test = "$length &gt; 0">
            <xsl:choose>
                <xsl:when test="substring-before($string,$guestsBorder) = ''">
                    <xsl:call-template name="getGuestInfo">
                        <xsl:with-param name="string" select = "$string"/>
                    </xsl:call-template>     
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="getGuestInfo">
                        <xsl:with-param name="string" select = "substring-before($string,$guestsBorder)"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:call-template name="getGuests">
                <xsl:with-param name="string" select = "substring-after($string,$guestsBorder)"/>
            </xsl:call-template>
        </xsl:if>     
    </xsl:template> 
    
    <xsl:template name="getGuestInfo">
        <xsl:param name="string"/>
        <Guest>  
            <xsl:attribute name="Age">
                <xsl:call-template name="getString">
                    <xsl:with-param name="string" select = "$string"/>
                    <xsl:with-param name="number" select = "'3'"/>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:attribute name="Nationalty">
                <xsl:call-template name="getString">
                    <xsl:with-param name="string" select = "$string"/>
                    <xsl:with-param name="number" select = "'4'"/>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:attribute name="Gender">
                <xsl:call-template name="getString">
                    <xsl:with-param name="string" select = "$string"/>
                    <xsl:with-param name="number" select = "'5'"/>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:attribute name="Name">
                <xsl:call-template name="getString">
                    <xsl:with-param name="string" select = "$string"/>
                    <xsl:with-param name="number" select = "'6'"/>
                </xsl:call-template>
            </xsl:attribute>
            <Type>
                <xsl:call-template name="getString">
                    <xsl:with-param name="string" select = "$string"/>
                    <xsl:with-param name="number" select = "'1'"/>
                </xsl:call-template>
            </Type>
            <Profile>
                <Address>
                    <xsl:call-template name="getString">
                        <xsl:with-param name="string" select = "$string"/>
                        <xsl:with-param name="number" select = "'2'"/>
                    </xsl:call-template>
                </Address>
            </Profile>     
        </Guest>
                     
    </xsl:template>    
    
    <xsl:template name="getString">
        <xsl:param name="string"/>
        <xsl:param name="number" select="1"/>
        <xsl:variable name="beforeString" select="substring-before($string,$infoBorder)"/>
        
        <xsl:choose>
            <xsl:when test="$number &gt; 1">
                <xsl:call-template name="getString">
                    <xsl:with-param name="string" select="substring-after($string,$infoBorder)"/>
                    <xsl:with-param name="number" select="$number -1"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>       
                    <xsl:when test="$beforeString = '' ">
                        <xsl:value-of select="$string"/>             
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$beforeString"/> 
                    </xsl:otherwise>            
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>
    
</xsl:stylesheet>


