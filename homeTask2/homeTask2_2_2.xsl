<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"> 
   <!--  Данный шаблон является не универсальным. Сделал также с рекурсией(homeTask2_2_2_recursion.xslt). --> 
       
    <xsl:template match="/">
        <Guests>
            <xsl:call-template name="getGuests">
                <xsl:with-param name="string" select  = "."/>
            </xsl:call-template>
        </Guests>
            
    </xsl:template>

    <xsl:template name ="getGuests">
        <xsl:param name="string"/>
        <xsl:variable name="guest1" select = "substring-before($string,'#')" />
        <xsl:variable name="guest2" select = "substring-before(substring-after($string,concat($guest1,'#')),'#')" />
        <xsl:variable name="guest3" select = "substring-before(substring-after($string,concat($guest1,'#',$guest2,'#')),'#')" />
        <xsl:variable name="guest4" select = "substring-before(substring-after($string,concat($guest1,'#',$guest2,'#',$guest3,'#')),'#')" />
        <xsl:variable name="guest5" select = "substring-before(substring-after($string,concat($guest1,'#',$guest2,'#',$guest3,'#',$guest4,'#')),'#')" />
        <xsl:variable name="guest6" select = "substring-before(substring-after($string,concat($guest1,'#',$guest2,'#',$guest3,'#',$guest4,'#',$guest5,'#')),'#')" />
        <xsl:variable name="guest7" select = "substring-before(substring-after($string,concat($guest1,'#',$guest2,'#',$guest3,'#',$guest4,'#',$guest5,'#',$guest6,'#')),'#')" />
        <xsl:variable name="guest8" select = "substring-before(substring-after($string,concat($guest1,'#',$guest2,'#',$guest3,'#',$guest4,'#',$guest5,'#',$guest6,'#',$guest7,'#')),'#')" />
        <xsl:variable name="guest9" select = "substring-after($string,concat($guest1,'#',$guest2,'#',$guest3,'#',$guest4,'#',$guest5,'#',$guest6,'#',$guest7,'#',$guest8,'#'))" />
        
        <xsl:call-template name="getGuestInfo">
            <xsl:with-param name="string" select="$guest1"/>
        </xsl:call-template>
        <xsl:call-template name="getGuestInfo">
            <xsl:with-param name="string" select="$guest2"/>
        </xsl:call-template>                
        <xsl:call-template name="getGuestInfo">
            <xsl:with-param name="string" select="$guest3"/>
        </xsl:call-template>
        <xsl:call-template name="getGuestInfo">
            <xsl:with-param name="string" select="$guest4"/>
        </xsl:call-template>
        <xsl:call-template name="getGuestInfo">
            <xsl:with-param name="string" select="$guest5"/>
        </xsl:call-template>
        <xsl:call-template name="getGuestInfo">
            <xsl:with-param name="string" select="$guest6"/>
        </xsl:call-template>
        <xsl:call-template name="getGuestInfo">
            <xsl:with-param name="string" select="$guest7"/>
        </xsl:call-template>
        <xsl:call-template name="getGuestInfo">
            <xsl:with-param name="string" select="$guest8"/>
        </xsl:call-template>
        <xsl:call-template name="getGuestInfo">
            <xsl:with-param name="string" select="$guest9"/>
        </xsl:call-template>
  
    </xsl:template>   
    
    <xsl:template name= "getGuestInfo">
        <xsl:param name="string"/>
        <Guest>  
            <xsl:attribute name="Age">
                <xsl:call-template name="getString">
                    <xsl:with-param name="string" select = "$string"/>
                    <xsl:with-param name="number" select = "'3'"/>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:attribute name="Nationalty">
                <xsl:call-template name="getString">
                    <xsl:with-param name="string" select = "$string"/>
                    <xsl:with-param name="number" select = "'4'"/>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:attribute name="Gender">
                <xsl:call-template name="getString">
                    <xsl:with-param name="string" select = "$string"/>
                    <xsl:with-param name="number" select = "'5'"/>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:attribute name="Name">
                <xsl:call-template name="getString">
                    <xsl:with-param name="string" select = "$string"/>
                    <xsl:with-param name="number" select = "'6'"/>
                </xsl:call-template>
            </xsl:attribute>
            <Type>
                <xsl:call-template name="getString">
                    <xsl:with-param name="string" select = "$string"/>
                    <xsl:with-param name="number" select = "'1'"/>
                </xsl:call-template>
            </Type>
            <Profile>
                <Address>
                    <xsl:call-template name="getString">
                        <xsl:with-param name="string" select = "$string"/>
                        <xsl:with-param name="number" select = "'2'"/>
                    </xsl:call-template>
                </Address>
            </Profile>           
        </Guest>
      
    </xsl:template>
       
    <xsl:template name = "getString">
            <xsl:param name="string"/>
            <xsl:param name="number"/>
            <xsl:variable name="type" select = "substring-before($string,'|')" />
            <xsl:variable name="address" select = "substring-before(substring-after($string,concat($type,'|')),'|')" />
            <xsl:variable name="age" select = "substring-before(substring-after($string,concat($type,'|',$address,'|')),'|')" />
            <xsl:variable name="nationality" select = "substring-before(substring-after($string,concat($type,'|',$address,'|',$age,'|')),'|')" />
            <xsl:variable name="gender" select = "substring-before(substring-after($string,concat($type,'|',$address,'|',$age,'|',$nationality,'|')),'|')" />
            <xsl:variable name="name" select = "substring-after($string,concat($type,'|',$address,'|',$age,'|',$nationality,'|',$gender,'|'))" />
            
            <xsl:choose>
                <xsl:when test="$number = 1">
                    <xsl:value-of select="$type"/>
                </xsl:when>
                <xsl:when test="$number = 2">
                    <xsl:value-of select="$address"/>
                </xsl:when>
                <xsl:when test="$number = 3">
                    <xsl:value-of select="$age"/>
                </xsl:when>
                <xsl:when test="$number = 4">
                    <xsl:value-of select="$nationality"/>
                </xsl:when>
                <xsl:when test="$number = 5">
                    <xsl:value-of select="$gender"/>
                </xsl:when>
                <xsl:when test="$number = 6">
                    <xsl:value-of select="$name"/>
                </xsl:when>
            </xsl:choose>
            
        </xsl:template>
            
</xsl:stylesheet>