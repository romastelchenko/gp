<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:h="HouseChema" xmlns:i="HouseInfo" xmlns:r="RoomsChema" version="1.0"> 
    
    <xsl:template match="h:Houses">
        
        <AllRoms>
           
            <xsl:for-each select="h:House">      
            <xsl:sort select="@City" data-type="text" />
                <xsl:variable name="city" select="translate(@City,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
                <xsl:variable name="add" select="i:Address"/>
                <xsl:variable name="all_guests" select ="sum(descendant::r:Room/@guests)"/>
                <xsl:variable name="room_count" select = "count(descendant::r:Room)"/>
                <xsl:variable name="mean" select="floor($all_guests div $room_count)"/>
                
                <xsl:for-each select="*/h:Block">
                <xsl:sort select="@number" data-type="text" />
                    <xsl:variable name="block_number" select="@number"/>
                    
                    <xsl:for-each select="r:Room | */r:Room">
                    <xsl:sort select="@nuber" data-type="number" />
                        <Room>
                            <Address>
                                <xsl:value-of select="concat($city,'/',$add,'/',$block_number,'/',@nuber)"/>
                            </Address>
                            <HouseRoomsCount><xsl:value-of select="$room_count"/></HouseRoomsCount>
                            <BlockRoomsCount><xsl:value-of select="count(following-sibling::*)+ 1 + count(preceding-sibling::*)"/></BlockRoomsCount>
                            <HouseGuestsCount><xsl:value-of select="$all_guests"/></HouseGuestsCount>
                            <GuestsPerRoomAverage><xsl:value-of select="$mean"/></GuestsPerRoomAverage>
                            <Allocated>
                                <xsl:attribute name="Single"><xsl:value-of select="@guests = '1'"/></xsl:attribute>
                                <xsl:attribute name="Double"><xsl:value-of select="@guests = '2'"/></xsl:attribute>
                                <xsl:attribute name="Triple"><xsl:value-of select="@guests = '3'"/></xsl:attribute>
                                <xsl:attribute name="Quarter"><xsl:value-of select="@guests = '4'"/></xsl:attribute>
                            </Allocated>
                        </Room>
                    </xsl:for-each>
                </xsl:for-each> 
            </xsl:for-each>
        </AllRoms>   
    </xsl:template>
  
</xsl:stylesheet>


